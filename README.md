# sytac-java-exercise


### Requirements.
In order to build and run the project you will need:
* JDK 8
* Maven 3
* Docker - Tested using version 18.06.0-ce, but older versions might work as well.

The exercise uses project Lombok to generate biolerplate code. Lombok plugin needs to be instealled in the IDE of choice to properly view the project.

### Build
Run maven clean and install goals to build the project.

### Run
The easiest way to run the project is by executing the Dockerfile contained therein:
* `docker build -t sytac-java-exercise .`
* `docker run -v /path/to/mount/point:/work/sytac/output/ --env CONSUMER_KEY=<consumer-key> --env CONSUMER_SECRET=<consumer-secret> --env TOKEN=<token> --env TOKEN_SECRET=<token-secret> --env KEYWORDS=<keywords> --name sytac-java-exercise sytac-java-exercise `.

After the container runs, a file called `twitter-stream.json` will be available at the mount point.

##### Docker volume
* `-v /path/to/mount/point:/work/sytac/output/`: Volume. It bounds a path in the host machine to a path inside the container. The output file generated in the container will be available in the host machine.

##### Docker environment variables
* `CONSUMER_KEY`: Consumer key used by the app to access the Twitter Streaming API.
* `CONSUMER_SECRET`: Consumer secret used by the app to access the Twitter Streaming API.
* `TOKEN`: Application token used by the app to access the Twitter Streaming API.
* `TOKEN_SECRET`: Application token secret used by the app to access the Twitter Streaming API.
* `KEYWORDS`: Comma-separated list of keywords.
* `TIMEOUT`: Maximum amount of time (in seconds) to filter twits from the streaming API.
* `MAX_TWITS`: Maximum number of twits to filter from the streaming API. 