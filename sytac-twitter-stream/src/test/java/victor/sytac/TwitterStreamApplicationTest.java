package victor.sytac;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import victor.sytac.exception.TwitterStreamingApplicationException;
import victor.sytac.exception.TwitterStreamingException;
import victor.sytac.service.TwitOutputService;
import victor.sytac.service.TwitterStreamService;

@ExtendWith(MockitoExtension.class)
class TwitterStreamApplicationTest {

  @Mock(answer = Answers.RETURNS_SELF)
  private TwitterStreamService.TwitterStreamServiceBuilder mockTwitterStreamServiceBuilder;

  @Mock private TwitterStreamService mockService;

  @Mock private TwitOutputService mockTwitOutputService;

  @Test
  void constructor_noConsumerKey_nullPointerException() {
    String[] args =
        new String[] {
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret"
        };

    NullPointerException npe =
        assertThrows(
            NullPointerException.class,
            () ->
                new TwitterStreamApplication(
                    args, mockTwitterStreamServiceBuilder, mockTwitOutputService));
    assertEquals("consumerKey", npe.getMessage());
  }

  @Test
  void constructor_noConsumerSecret_nullPointerException() {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret"
        };

    NullPointerException npe =
        assertThrows(
            NullPointerException.class,
            () ->
                new TwitterStreamApplication(
                    args, mockTwitterStreamServiceBuilder, mockTwitOutputService));
    assertEquals("consumerSecret", npe.getMessage());
  }

  @Test
  void constructor_noToken_nullPointerException() {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token-secret=dummy_token_secret"
        };

    NullPointerException npe =
        assertThrows(
            NullPointerException.class,
            () ->
                new TwitterStreamApplication(
                    args, mockTwitterStreamServiceBuilder, mockTwitOutputService));
    assertEquals("token", npe.getMessage());
  }

  @Test
  void constructor_noTokenSecret_nullPointerException() {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token"
        };

    NullPointerException npe =
        assertThrows(
            NullPointerException.class,
            () ->
                new TwitterStreamApplication(
                    args, mockTwitterStreamServiceBuilder, mockTwitOutputService));
    assertEquals("tokenSecret", npe.getMessage());
  }

  @Test
  void constructor_allConfigArgs_defaults() throws TwitterStreamingApplicationException {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret"
        };

    doReturn(mockService).when(mockTwitterStreamServiceBuilder).build();

    TwitterStreamApplication app =
        new TwitterStreamApplication(args, mockTwitterStreamServiceBuilder, mockTwitOutputService);
    app.start();

    verify(mockService)
        .filter(
            TimeUnit.SECONDS.toMillis(TwitterStreamApplication.DEFAULT_TIMEOUT_SECONDS),
            TwitterStreamApplication.DEFAULT_MAX_TWITS);
  }

  @Test
  void constructor_someTimout_correctTimeout() throws TwitterStreamingApplicationException {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret",
          "--timeout=120"
        };

    doReturn(mockService).when(mockTwitterStreamServiceBuilder).build();

    TwitterStreamApplication app =
        new TwitterStreamApplication(args, mockTwitterStreamServiceBuilder, mockTwitOutputService);
    app.start();

    verify(mockService)
        .filter(TimeUnit.SECONDS.toMillis(120), TwitterStreamApplication.DEFAULT_MAX_TWITS);
  }

  @Test
  void constructor_someMaxTweets_correctMaxTweets() throws TwitterStreamingApplicationException {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret",
          "--max=250"
        };

    doReturn(mockService).when(mockTwitterStreamServiceBuilder).build();

    TwitterStreamApplication app =
        new TwitterStreamApplication(args, mockTwitterStreamServiceBuilder, mockTwitOutputService);
    app.start();

    verify(mockService)
        .filter(TimeUnit.SECONDS.toMillis(TwitterStreamApplication.DEFAULT_TIMEOUT_SECONDS), 250);
  }

  @Test
  void constructor_someKeywords_correctKeywords() throws TwitterStreamingApplicationException {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret",
          "--keywords=dummy,test,keywords"
        };

    doReturn(mockService).when(mockTwitterStreamServiceBuilder).build();

    TwitterStreamApplication app =
        new TwitterStreamApplication(args, mockTwitterStreamServiceBuilder, mockTwitOutputService);
    app.start();

    verify(mockService)
        .filter(
            TimeUnit.SECONDS.toMillis(TwitterStreamApplication.DEFAULT_TIMEOUT_SECONDS),
            TwitterStreamApplication.DEFAULT_MAX_TWITS,
            "dummy",
            "test",
            "keywords");
  }

  @Test
  void constructor_emptyOptionalParams_defaults() throws TwitterStreamingApplicationException {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret",
          "--timeout=",
          "--max=",
          "--keywords="
        };

    doReturn(mockService).when(mockTwitterStreamServiceBuilder).build();

    TwitterStreamApplication app =
        new TwitterStreamApplication(args, mockTwitterStreamServiceBuilder, mockTwitOutputService);
    app.start();

    verify(mockService)
        .filter(
            TimeUnit.SECONDS.toMillis(TwitterStreamApplication.DEFAULT_TIMEOUT_SECONDS),
            TwitterStreamApplication.DEFAULT_MAX_TWITS);
  }

  @Test
  void constructor_maxTwitsTooLow_defaultValue() throws TwitterStreamingApplicationException {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret",
          "--max=-1"
        };

    doReturn(mockService).when(mockTwitterStreamServiceBuilder).build();

    TwitterStreamApplication app =
        new TwitterStreamApplication(args, mockTwitterStreamServiceBuilder, mockTwitOutputService);
    app.start();

    verify(mockService)
        .filter(TimeUnit.SECONDS.toMillis(TwitterStreamApplication.DEFAULT_TIMEOUT_SECONDS), -1);
  }

  @Test
  void constructor_maxTwitsTooHigh_defaultValue() throws TwitterStreamingApplicationException {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret",
          "--max=100000"
        };

    doReturn(mockService).when(mockTwitterStreamServiceBuilder).build();

    TwitterStreamApplication app =
        new TwitterStreamApplication(args, mockTwitterStreamServiceBuilder, mockTwitOutputService);
    app.start();

    verify(mockService)
        .filter(
            TimeUnit.SECONDS.toMillis(TwitterStreamApplication.DEFAULT_TIMEOUT_SECONDS), 100_000);
  }

  @Test
  void constructor_allParams_success() throws TwitterStreamingApplicationException {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret",
          "--timeout=600",
          "--max=1000",
          "--keywords=dummy"
        };

    doReturn(mockService).when(mockTwitterStreamServiceBuilder).build();

    TwitterStreamApplication app =
        new TwitterStreamApplication(args, mockTwitterStreamServiceBuilder, mockTwitOutputService);
    app.start();

    verify(mockService).filter(TimeUnit.SECONDS.toMillis(600), 1_000, "dummy");
  }

  @Test
  void constructor_startException_fail() throws TwitterStreamingApplicationException {
    String[] args =
        new String[] {
          "--consumer-key=dummy_consumer_key",
          "--consumer-secret=dummy_consumer_secret",
          "--token=dummy_token",
          "--token-secret=dummy_token_secret",
        };

    doReturn(mockService).when(mockTwitterStreamServiceBuilder).build();
    doThrow(new TwitterStreamingException("Some error"))
        .when(mockService)
        .filter(anyLong(), anyInt());

    TwitterStreamApplication app =
        new TwitterStreamApplication(args, mockTwitterStreamServiceBuilder, mockTwitOutputService);

    assertThrows(TwitterStreamingApplicationException.class, app::start);
  }
}
