package victor.sytac.json.input;

import com.google.gson.annotations.SerializedName;
import java.util.Date;
import lombok.Value;

/** Class that maps to a single twit message. */
@Value
public class Twit {

  @SerializedName("id_str")
  private String twitterMessageId;

  @SerializedName("text")
  private String text;

  @SerializedName("created_at")
  private Date creationDate;

  @SerializedName("user")
  private TwitterUser author;
}
