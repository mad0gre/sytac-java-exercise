package victor.sytac.json.input;

import com.google.gson.annotations.SerializedName;
import java.util.Date;
import lombok.Value;

/** Class that maps to a Twitter User. */
@Value
public class TwitterUser {

  @SerializedName("id_str")
  private String twitterUserId;

  @SerializedName("created_at")
  private Date createdAt;

  @SerializedName("name")
  private String name;

  @SerializedName("screen_name")
  private String screenName;
}
