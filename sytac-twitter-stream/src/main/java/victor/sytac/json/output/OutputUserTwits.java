package victor.sytac.json.output;

import java.util.Collection;
import java.util.Date;
import lombok.Value;
import victor.sytac.json.input.TwitterUser;

/** Class used to output twits per user to JSON format. */
@Value
public class OutputUserTwits {
  private String twitterUserId;
  private Date createdAt;
  private String name;
  private String screenName;
  private Collection<OutputTwitMessage> twits;

  public OutputUserTwits(TwitterUser twitterUser, Collection<OutputTwitMessage> outputTwitMessages) {
    this.twitterUserId = twitterUser.getTwitterUserId();
    this.createdAt = twitterUser.getCreatedAt();
    this.name = twitterUser.getName();
    this.screenName = twitterUser.getScreenName();

    this.twits = outputTwitMessages;
  }
}
