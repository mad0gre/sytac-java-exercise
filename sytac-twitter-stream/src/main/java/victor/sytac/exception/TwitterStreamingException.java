package victor.sytac.exception;

/** Exception used for errors when streaming Tweets. */
public class TwitterStreamingException extends TwitterStreamingApplicationException {

  /** @param message Error message for exception. */
  public TwitterStreamingException(String message) {
    super(message);
  }

  /**
   * @param message Error message for exception.
   * @param cause {@link Throwable} object that this exception encapsulates.
   */
  public TwitterStreamingException(String message, Throwable cause) {
    super(message, cause);
  }
}
